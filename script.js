function showPassword(){
	let firstBtn = document.getElementById('first-btn');
	let firstPW = document.getElementById('first-pw');
	let secondBtn = document.getElementById('second-btn');
	let secondPW = document.getElementById('second-pw');
	let submit = document.querySelector('.btn');
	let error = document.querySelector('.error');

    firstBtn.addEventListener('click', () => {
		if (firstBtn.classList.contains('fa-eye')){
			firstBtn.classList.remove('fa-eye');
			firstBtn.classList.add('fa-eye-slash');
		}
		else if (firstBtn.classList.contains('fa-eye-slash')){
			firstBtn.classList.remove('fa-eye-slash');
			firstBtn.classList.add('fa-eye');
		}
		
		if (firstPW.getAttribute('type') === 'password'){
			firstPW.setAttribute('type', 'text');
		}
		else {
			firstPW.setAttribute('type', 'password');
		}
	});

	secondBtn.addEventListener('click', () => {
		if (secondBtn.classList.contains('fa-eye')){
			secondBtn.classList.remove('fa-eye');
			secondBtn.classList.add('fa-eye-slash');
		}
		else if (secondBtn.classList.contains('fa-eye-slash')){
			secondBtn.classList.remove('fa-eye-slash');
			secondBtn.classList.add('fa-eye');
		}
		
		if (secondPW.getAttribute('type') === 'password'){
			secondPW.setAttribute('type', 'text');
		}
		else {
			secondPW.setAttribute('type', 'password');
		}
	});

	submit.addEventListener('click', () =>{
		if (firstPW.value == secondPW.value){
			alert('You are welcome');
		}
		else {
			error.hidden = false;
		}
	});
}

showPassword();